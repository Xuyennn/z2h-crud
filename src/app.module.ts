import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GameController } from './game/game.controller';
import { GameService } from './game/game.service';
import { MongooseModule as mongoose } from '@nestjs/mongoose'
import { GameModule } from './game/game.module';
import { env } from 'process';

@Module({
  imports:  [ mongoose.forRoot('mongodb+srv://test:123456aa@cluster0.frcrg.mongodb.net/nhanvat?retryWrites=true&w=majority', { useCreateIndex: true }),
  GameModule 
 ],
  controllers: [AppController],
  providers: [AppService   
  ],
})
export class AppModule {}
