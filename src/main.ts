import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import 'dotenv/config' ;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
  .setTitle('My Swagger')
  .setDescription('Api description')
  .setVersion('0.1')
  .addTag('Day la cac API')
  .build();
  
  const document = SwaggerModule.createDocument(app , options);
  SwaggerModule.setup('api', app, document);

  app.enableCors();
  
  await app.listen(process.env.PORT ||  5000);
}
bootstrap();
