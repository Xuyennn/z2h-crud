import { DocumentNames } from './../shared/enums/document-names.enum';
import { NhanVatSchema } from './Schema/NhanVat.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { GameController } from './game.controller';
import { Module } from '@nestjs/common';
import { GameService } from './game.service';
import { RepoNhanVat } from './Schema/Repository';

@Module({
    imports : [ MongooseModule.forFeature([{ name : DocumentNames.Nhanvat , schema : NhanVatSchema }])] ,

    controllers : [
        GameController
    ],
    providers : [
        {
            provide: 'iNhanVat',
            useClass : GameService
          }, 
          RepoNhanVat
    ],
})
export class GameModule {}
