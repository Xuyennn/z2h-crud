import { Nhanvat } from './../Model/nhanvat';
import { InjectModel } from '@nestjs/mongoose';
import { DocumentNames } from 'src/shared/enums/document-names.enum';
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { CreateNhanvatDto } from '../Model/create-nhanvat.dto';

@Injectable()
export class RepoNhanVat {
  constructor(
    @InjectModel(DocumentNames.Nhanvat)
    private readonly nhanvatRepo: Model<Nhanvat>,
  ) {}

  async getAll(): Promise<Nhanvat[]> {
    const kq = await this.nhanvatRepo.find();
    return kq;    
  }
  async getByBang(tenbang: string): Promise<Nhanvat[]> {
    return this.nhanvatRepo.find({ bang: { $regex: `.*${tenbang}.*` } });
  }

  async createNhanvat(nv: CreateNhanvatDto): Promise<Nhanvat> {    
    return await this.nhanvatRepo.create(nv);
  }

  async updateById(id: string, nv: Nhanvat): Promise<Nhanvat> {
    return this.nhanvatRepo.findByIdAndUpdate(id, nv, { new: true });
  }

  async deleteById(id: string): Promise<Nhanvat> {
    return await this.nhanvatRepo.findByIdAndDelete({ _id: id });
  }
}
