import * as mongoose from 'mongoose'; 
import { Schema } from 'mongoose';

export const NhanVatSchema = new Schema({
    id : { type: String },
    tenServer : { type: String} ,
    tenNhanVat : { type: String, unique: true } ,
    bang : { type: String},
});
