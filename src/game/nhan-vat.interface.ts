import { Nhanvat } from './Model/nhanvat';
import { CreateNhanvatDto } from './Model/create-nhanvat.dto';
export interface  INhanVat {    
     getFull() : Promise<Nhanvat[]> ;
     getByBang(ten : string) : Promise<Nhanvat[]>; 
     createNhanvat(nhanvat : CreateNhanvatDto) : Promise<Nhanvat> ;
     updateById(id : string , nv : Nhanvat ) : Promise<Nhanvat>;
     deleteById(id : string ) : Promise<Nhanvat>;
}