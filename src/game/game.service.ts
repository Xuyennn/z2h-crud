import { StateMachine, interpret } from 'xstate';
import { Injectable, BadRequestException, Scope } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { INhanVat } from './nhan-vat.interface';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Console } from 'console';
import { DocumentNames } from 'src/shared/enums/document-names.enum';
import { doc } from 'prettier';
import { Nhanvat } from './Model/nhanvat';
import { CreateNhanvatDto } from './Model/create-nhanvat.dto';
import { Machine, MachineConfig } from 'xstate';
import { from } from 'rxjs';
import { RepoNhanVat } from './Schema/Repository';
import * as clc from 'cli-color';

@Injectable({ scope: Scope.REQUEST })
export class GameService implements INhanVat {
  stateMC; //: StateMachine<any,any, any ,any>;
  stateMy = {};
  constructor(
    @InjectModel(DocumentNames.Nhanvat)
    private readonly nhanvatRepo: Model<Nhanvat>,
    private readonly repo: RepoNhanVat,
  ) {
    this.stateMC = interpret(this.nhanvatMachine)
      .onTransition(state => console.log(clc.blueBright("State : " , state.value )))
      .start();
  }
  async getFull(): Promise<Nhanvat[]> {
    this.stateMC.send('GetAll');    
    const kq = await this.repo.getAll()
                .then((data) => {
                  this.stateMC.send('GET_OK');
                  return data;})
                // .then(data => {
                //   this.stateMC.send('BACK');
                //   return data;}) /// no need when using scope by request
                .catch(err => {
                  this.stateMC.send('GET_FAIL');
                  console.log(err);
                  //this.stateMC.send('BACK') ;
                  return null});    
    return kq;
  }

  async getByBang(tenbang: string): Promise<Nhanvat[]> {
    return await this.repo.getByBang(tenbang);
  }

  async createNhanvat(nv: CreateNhanvatDto): Promise<Nhanvat> {
    //nhanvat._id = this.uuidv4() ;
    this.stateMC.send('CREATE');
    const kq =  await this.repo.createNhanvat(nv)
                      .then( data => {
                        this.stateMC.send('CREATE_OK');
                        return data ;
                      })                      
                      .catch( err => {
                        this.stateMC.send('CREATE_FAIL');                        
                        return err;
                      });
    return kq;
  }

  async updateById(id: string, nv: Nhanvat): Promise<Nhanvat> {
    this.stateMC.send('UPDATE');
    const kq = this.repo.updateById(id, nv)
                      .then( data => {
                        this.stateMC.send('UPDATE_OK');                        
                        return data ;
                      })
                      .catch( err => {
                        this.stateMC.send('UPDATE_FAIL');                        
                        return err ;
                      });
    return kq;
  }

  async deleteById(id: string): Promise<Nhanvat> {
    this.stateMC.send('DELETE');
    const kq = await this.repo.deleteById(id)
                        .then( data => {
                          this.stateMC.send('DELETE_OK');                          
                          return data ;
                        })
                        .catch( err => {
                          this.stateMC.send('DELETE_FAIL');                          
                          return err ;
                        });
    return kq ;                        
  }

  uuidv4(): string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (Math.random() * 16) | 0,
        v = c == 'x' ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  }
  nhanvatMachine = Machine({
    id: 'fetch',
    initial: 'idle',
    states: {
      idle: {
        on: {
          GetAll: 'getAll',
          CREATE: 'create',
          UPDATE: 'update',
          DELETE: 'delete',
        },
      },      
      getAll: {
        on: {
          GET_OK: 'success',
          GET_FAIL: 'failure',
        },
      },
      create: {
        on: {
          CREATE_OK: 'createok',
          CREATE_FAIL: 'createFail',
        },
      },
      createok: {
        // on: {
        //   rt: {
        //     target: 'waitingTrigger',
        //     // actions :'logMessage'
        //   },
        // },
        type: 'final'
      },
      createFail: {
        // on: {
        //   rt: {
        //     target: 'waitingTrigger',
        //     // actions :'logMessage'
        //   },
        // },
        type: 'final'
      },
      update: {
        on: {
          UPDATE_OK: 'updateok',
          UPDATE_FAIL: 'updateFail',
        },
      },
      updateok: {
        // on: {
        //   BACK: {
        //     target: 'waitingTrigger',
        //     // actions :'logMessage'
        //   },
        // },
        type : 'final'
      },
      updateFail: {
        // on: {
        //   BACK: {
        //     target: 'waitingTrigger',
        //     // actions :'logMessage'
        //   },
        // },
        type : 'final'
      },
      delete: {
        on: {
          DELETE_OK: 'deleteok',
          DELETE_FAIL: 'deleteFail',
        },
      },
      deleteok: {
        // on: {
        //   BACK: {
        //     target: 'waitingTrigger',
        //     // actions :'logMessage'
        //   },
        // },
        type : 'final'
      },
      deleteFail: {
        // on: {
        //   BACK: {
        //     target: 'waitingTrigger',
        //     // actions :'logMessage'
        //   },
        // },
        type : 'final'
      },
      success: {
        // on: {
        //   BACK: {
        //     target: 'waitingTrigger',
        //     // actions :'logMessage'
        //   },
        // },
        type : 'final'
      },
      failure: {
        // on: {
        //   BACK: {
        //     target: 'waitingTrigger',
        //     // actions :'logMessage'
        //   },
        // },
        type : 'final'
      },
    },
  });
}
