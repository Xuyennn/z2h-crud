import { GameService } from './game.service';
import { Controller, Get, Body , Post, Param, Injectable, Inject, Put, Delete } from '@nestjs/common';
import { Nhanvat } from './Model/nhanvat';
import { ApiTags, ApiParam } from '@nestjs/swagger';
import { CreateNhanvatDto } from './Model/create-nhanvat.dto';

@ApiTags('API Game')
@Controller('game')
export class GameController {
    constructor(
        @Inject('iNhanVat')
        private gameService : GameService
    ){}

    @Get()
    async getTest() : Promise<Nhanvat[]> {
       return await this.gameService.getFull() ;
    }

    //@ApiParam({name : 'nv', type : 'NhanVat'}) //: chi nen dung cho get
    @Post()
    async receivedFromPM(@Body() nv : CreateNhanvatDto) : Promise<Nhanvat> {
        console.log(nv);
      return this.gameService.createNhanvat(nv);    
    }
    
    @ApiParam({name : 'bang'})
    @Get(':bang')
    async getByID(@Param('bang') para : string) : Promise<Nhanvat[]> {    
        return await this.gameService.getByBang(para );
    }

    @ApiParam({name : 'id'})
    @Put(':id')
    async updateById(@Param('id') id :string , @Body() nv : Nhanvat) : Promise<Nhanvat> {
        nv.id = id;    
        return this.gameService.updateById(id , nv);
    }
    
    @ApiParam({name :'id'})
    @Delete(':id')
    async deleteById(@Param('id') id : string) : Promise<Nhanvat>{
        return this.gameService.deleteById(id);
    }
}
