import { ApiProperty } from "@nestjs/swagger";
import { Document } from 'mongoose';
import { nhanvatMachine } from "./nhanvat.statemachine";
import { interpret, Machine, StateMachine } from "xstate";


export class Nhanvat extends Document {
    // stateMachine ;
    // constructor(){
    //     super()
    //     this.stateMachine = interpret(nhanvatMachine).onTransition(state => console.log(state.value)).start();
    //     console.log(this.stateMachine);
    // }

    @ApiProperty()
    id : string ;
    @ApiProperty()
    tenServer : string ;

    @ApiProperty()
    tenNhanVat : string ;

    @ApiProperty()
    bang : string;

    // nhanvatMachine = Machine({
    //     id: 'fetch',
    //     initial: 'idle',
    //     states: {
    //       idle: {
    //         on: {
    //           GetAll: 'getAll',
    //           CREATE : 'create',
    //           UPDATE : 'update',
    //           DELETE : 'delete'
    //         }
    //       },
    //       getAll: {
    //         on: {
    //           getok: 'success',
    //           getfail: 'failure'
    //         }
    //       },
    //       create : {
    //         on : {
    //           createOk : 'createok',
    //           createFail : 'createFail'
    //         }
    //       },
    //       createok: {
    //         type : 'final'
    //       },
    //       createFail : {
    //         type : 'final'
    //       },
    //       update : {
    //         on : {
    //           updateOk : 'updateok',
    //           updateFail : 'updateFail'
    //         }
    //       },
    //       updateok: {
    //         type : 'final'
    //       },
    //       updateFail : {
    //         type : 'final'
    //       },
    //       delete : {
    //         on : {
    //           deleteOk : 'deleteok',
    //           deleteFail : 'deleteFail'
    //         }
    //       },
    //       deleteok: {
    //         type : 'final'
    //       },
    //       deleteFail : {
    //         type : 'final'
    //       },
    //       success: {
    //         type: 'final'
    //       },
    //       failure: {
    //         type : 'final'
    //       }
    //     }
    //   });
}