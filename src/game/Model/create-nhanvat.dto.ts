import { ApiProperty } from "@nestjs/swagger";

export  class CreateNhanvatDto {
    //_id : string ;

    @ApiProperty()
    tenServer: string;

    @ApiProperty()
    tenNhanVat: string;

    @ApiProperty()
    bang: string;    
}
