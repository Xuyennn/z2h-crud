import {Machine } from 'xstate'

export const nhanvatMachine = Machine({
    id: 'fetch',
    initial: 'idle',
    states: {
      idle: {
        on: {
          GetAll: 'getAll',
          CREATE : 'create',
          UPDATE : 'update',
          DELETE : 'delete'
        }
      },
      getAll: {
        on: {
          getok: 'success',
          getfail: 'failure'
        }
      },
      create : {
        on : {
          createOk : 'createok',
          createFail : 'createFail'
        }
      },
      createok: {
        type : 'final'
      },
      createFail : {
        type : 'final'
      },
      update : {
        on : {
          updateOk : 'updateok',
          updateFail : 'updateFail'
        }
      },
      updateok: {
        type : 'final'
      },
      updateFail : {
        type : 'final'
      },
      delete : {
        on : {
          deleteOk : 'deleteok',
          deleteFail : 'deleteFail'
        }
      },
      deleteok: {
        type : 'final'
      },
      deleteFail : {
        type : 'final'
      },
      success: {
        type: 'final'
      },
      failure: {
        type : 'final'
      }
    }
  });